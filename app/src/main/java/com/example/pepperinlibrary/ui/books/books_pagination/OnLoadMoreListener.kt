package com.example.pepperinlibrary.ui.books.books_pagination

interface OnLoadMoreListener {
    fun onLoadMore()
}