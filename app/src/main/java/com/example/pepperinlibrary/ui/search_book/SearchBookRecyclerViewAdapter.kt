package com.example.pepperinlibrary.ui.search_book

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.tools.extensions.setGlideImage
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import com.example.pepperinlibrary.ui.books.BooksModel
import kotlinx.android.synthetic.main.books_search_rv_layout.view.*

class SearchBookRecyclerViewAdapter(
    val books: MutableList<BooksModel.Items>,
    val customOnClick: CustomOnClick
) :
    RecyclerView.Adapter<SearchBookRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.books_search_rv_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = books.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: BooksModel.Items
        fun onBind() {
            model = books[adapterPosition]
            itemView.searchTitleTV.text = model.volumeInfo.title
            itemView.searchBooksImageView.setGlideImage(model.volumeInfo.imageLinks.thumbnail)
            if (model.volumeInfo.authors.isNotEmpty())
                itemView.searchAuthorTV.text = model.volumeInfo.authors[0]
            itemView.searchBooksContainer.setOnClickListener {
                customOnClick.onClick(model.id)
            }
        }
    }
}