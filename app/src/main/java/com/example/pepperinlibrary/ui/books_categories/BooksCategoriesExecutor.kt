package com.example.pepperinlibrary.ui.books_categories

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor
import com.example.pepperinlibrary.App
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.Tools
import com.example.pepperinlibrary.ui.books.BooksActivity

class BooksCategoriesExecutor(context: QiContext) : BaseQiChatExecutor(context) {

    override fun runWith(params: MutableList<String>) {
        if (params.isNotEmpty()) {
            val intent = Intent(App.context, BooksActivity::class.java)
            SharedPreference.instance().saveString(SharedPreference.CATEGORY, params[0])
            startActivity(qiContext, intent, null)
            params.clear()
        }
    }

    override fun stop() {
        Log.d("TAGggg", "execute stopped")
    }
}