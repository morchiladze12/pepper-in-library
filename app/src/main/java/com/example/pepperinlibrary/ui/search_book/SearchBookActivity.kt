package com.example.pepperinlibrary.ui.search_book

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.data_loader.CustomCallback
import com.example.pepperinlibrary.network.data_loader.HttpRequest
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.Tools
import com.example.pepperinlibrary.tools.extensions.hideKeyboard
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import com.example.pepperinlibrary.ui.books.BooksActivity
import com.example.pepperinlibrary.ui.books.BooksActivity.Companion.currentBook
import com.example.pepperinlibrary.ui.books.BooksModel
import com.example.pepperinlibrary.ui.borrow_book.BorrowBookActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_search_book.*

class SearchBookActivity : RobotActivity(), RobotLifecycleCallbacks {

    private var books = mutableListOf<BooksModel.Items>()
    private lateinit var adapter: SearchBookRecyclerViewAdapter

    companion object {
        private var qiChatBot: QiChatbot? = null
        private var sayFuture: Future<Void>? = null
        var searchInputText = ""
        var say2: Say? = null
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_book)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)
        QiSDK.register(this, this)
        init()

        backButton.setOnClickListener {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
    }

    private fun init() {
        adapter = SearchBookRecyclerViewAdapter(books, object : CustomOnClick {
            override fun onClick(position: String) {
                super.onClick(position)
                if (say2 != null) {
                    sayFuture = say2!!.async().run()
                }
                getBookDetails(position, qiChatBot)
            }
        })
        booksSearchRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        booksSearchRV.adapter = adapter


        searchBooksEditText.addTextChangedListener(textWatcher)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            getBooks(p0.toString())
        }

    }

    private fun getBookDetails(bookId: String, qiChatBot: QiChatbot?) {
        HttpRequest.getRequest(bookId, "1",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    d("responseDet", response)
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    searchBooksEditText.hideKeyboard()
                    if (booksModel.items.isNotEmpty()) {

                        Tools.booksDetailsDialog(
                            this@SearchBookActivity,
                            booksModel.items[0],
                            qiChatBot,
                            this@SearchBookActivity
                        )
                    }

                }

                override fun onFailure(fail: String) {

                }
            })
    }

    private fun getBooks(input: String) {
        books.clear()
        if (input.isEmpty())
            return
        HttpRequest.getRequest(
            input, "8",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    books.clear()
                    if (input.isEmpty())
                        return
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    booksModel.items.forEach {
                        books.add(it)
                    }
                    adapter.notifyDataSetChanged()
                }

                override fun onFailure(fail: String) {

                }
            })
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {
        say2 = SayBuilder.with(qiContext)
            .withText("Good choice ! We have that book on the shelves, You can borrow it right now !")
            .build()

        val say: Say = SayBuilder.with(qiContext)
            .withText("You can write down your book name here !")
            .build()
        say.run()


        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.books)
            .build()

        qiChatBot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic)
            .build()

        val chat: Chat = ChatBuilder
            .with(qiContext)
            .withChatbot(qiChatBot)
            .build()

//        val executors = hashMapOf<String, QiChatExecutor>()
//        executors["search_book"] = SearchBooksExecutor(qiContext!!)
//        qiChatBot!!.executors = executors

        qiChatBot!!.addOnBookmarkReachedListener { bookmark: Bookmark ->
            when (bookmark.name) {
                "BACK" -> {
                    this.finish()
                }
                "BOOKS_SEARCH" -> {
                    if (searchInputText.isNotEmpty())
                        getBooks(searchInputText)
                }
                "YES" -> {
                    getBookDetails(BooksActivity.currentBook!!.id, qiChatBot)
                    val booksDetailsVariable: QiChatVariable =
                        qiChatBot!!.variable("books_details")
                    var desc = currentBook!!.volumeInfo.description
                    booksDetailsVariable.value = desc
                    booksDetailsVariable.addOnValueChangedListener {
                        desc = ""
                        d("desc", it)
                    }
                }
                "NO" -> {
                    currentBook = null
                }
                "EXIT" -> {
                    currentBook = null
                }
                "BORROW_BOOK" -> {
                    SharedPreference.instance()
                        .saveString(SharedPreference.BORROW_BOOK_ID, currentBook!!.id)
                    Tools.startNewActivity(this, BorrowBookActivity::class.java)
                }

            }
        }
        chat.run()
    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {
    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }
}