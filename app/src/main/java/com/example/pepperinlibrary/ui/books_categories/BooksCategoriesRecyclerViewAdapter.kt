package com.example.pepperinlibrary.ui.books_categories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.tools.extensions.setGlideImage
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import kotlinx.android.synthetic.main.categories_rv_layout.view.*

class BooksCategoriesRecyclerViewAdapter(val categories: MutableList<BooksCategoriesModel>, val customOnClick: CustomOnClick) :
    RecyclerView.Adapter<BooksCategoriesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.categories_rv_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var model: BooksCategoriesModel
        fun onBind() {
            model = categories[adapterPosition]
            itemView.categoriesTV.text = model.category
            itemView.categoryImageView.setGlideImage(model.imgUrl)
            itemView.categoriesContainer.setOnClickListener {
                customOnClick.onClick(model.category)
            }
        }
    }
}