package com.example.pepperinlibrary.ui.splash_screen

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.core.view.isVisible
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.Bookmark
import com.aldebaran.qi.sdk.builder.*
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.tools.Tools
import com.example.pepperinlibrary.tools.extensions.visible
import com.example.pepperinlibrary.ui.books_categories.BooksCategoriesActivity
import com.example.pepperinlibrary.ui.borrow_book.BorrowBookActivity
import com.example.pepperinlibrary.ui.scan_book.ScanBookActivity
import com.example.pepperinlibrary.ui.search_book.SearchBookActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.android.synthetic.main.main_menu_bottom_sheet_layout.*
import kotlinx.android.synthetic.main.main_menu_bottom_sheet_layout.categoriesImageView
import kotlinx.android.synthetic.main.main_menu_bottom_sheet_layout.searchBookImageView

class SplashScreenActivity : RobotActivity(), RobotLifecycleCallbacks {
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)
        QiSDK.register(this, this)
        initBottomSheet()
        onClickListeners()
    }

    private fun onClickListeners(){
        menuFloatingActionButton.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                menuFloatingActionButton.visible(false)
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                menuFloatingActionButton.visible(true)
            }
        }
        scanBookImageView.setOnClickListener {
            Tools.startNewActivity(this, ScanBookActivity::class.java)
        }
        categoriesImageView.setOnClickListener {
            Tools.startNewActivity(this, BooksCategoriesActivity::class.java)
        }
        searchBookImageView.setOnClickListener {
            Tools.startNewActivity(this, SearchBookActivity::class.java)
        }
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(mainMenuBottomSheetLayout)
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        menuFloatingActionButton.visible(false)
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        menuFloatingActionButton.visible(true)
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(view: View, p1: Float) {
            }
        })
    }




    override fun onRobotFocusGained(qiContext: QiContext?) {
        val topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.splash_screen_chat)
            .build()

        val chatBot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic).build()

        val chat = ChatBuilder
            .with(qiContext)
            .withChatbot(chatBot).build()

        val myAnimation = AnimationBuilder.with(qiContext)
            .withResources(R.raw.bowing_b001)
            .build()

        val animate = AnimateBuilder.with(qiContext)
            .withAnimation(myAnimation)
            .build()


        chatBot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            when (bookmark.name) {
                "BOOK_SEARCH" -> {
                    this.runOnUiThread {
                        Tools.startNewActivity(this, SearchBookActivity::class.java)
                    }
                }
                "BOOK_GENRES" -> {
                    this.runOnUiThread {
                        Tools.startNewActivity(this, BooksCategoriesActivity::class.java)
                    }
                }
                "BORROW_BOOK" -> {
                    this.runOnUiThread {
                        Tools.startNewActivity(this, ScanBookActivity::class.java)
                    }
                }
                "SHOW_MENU" -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
                "HELLO"->{
                    animate.async().run()
                }
            }

        }
        chat.async().run()

    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {

    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }

}