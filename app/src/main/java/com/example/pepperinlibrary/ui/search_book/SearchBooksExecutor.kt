package com.example.pepperinlibrary.ui.search_book

import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor

class SearchBooksExecutor(context: QiContext) : BaseQiChatExecutor(context) {

    override fun runWith(params: MutableList<String>) {
        Log.d("TAGggg", "Arm raised = ${params[0]}")
        if (params.isNotEmpty()) {
            SearchBookActivity.searchInputText = params[0]
        }
    }

    override fun stop() {
        Log.d("TAGggg", "execute stopped")
    }
}