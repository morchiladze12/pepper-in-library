package com.example.pepperinlibrary.ui.books.books_pagination

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.tools.extensions.setGlideImage
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import com.example.pepperinlibrary.ui.books.BooksModel
import kotlinx.android.synthetic.main.books_rv_layout.view.*

class PaginationBooksAdapter(
    private val recyclerView: RecyclerView,
    private val books: MutableList<BooksModel.Items>,
    val onLoadMoreListener: OnLoadMoreListener,
    private val customClick: CustomOnClick
    ) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        companion object {
            const val LOADING_ITEM = 2
            const val LIST_ITEM = 1
        }


        private var isLoading = false
        private var lastVisibleItem: Int = 0
        private var totalItemCount: Int = 0
        private val visibleThreshold = 4

        init {
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    totalItemCount = linearLayoutManager.itemCount
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                    if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                        onLoadMoreListener.onLoadMore()
                        isLoading = true
                    }
                }
            })
        }


        override fun getItemCount() = books.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is ViewHolder)
                holder.onBind()
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            lateinit var model: BooksModel.Items
            fun onBind() {
                model = books[adapterPosition]
                itemView.titleTV.text = model.volumeInfo.title
                itemView.booksImageView.setGlideImage(model.volumeInfo.imageLinks.thumbnail)
                if (model.volumeInfo.authors.isNotEmpty())
                    itemView.authorTV.text = model.volumeInfo.authors[0]
                itemView.booksContainer.setOnClickListener {
                    customClick.onClick(model.id)

                }

            }
        }

        inner class LoadMorePostsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

        fun setLoaded() {
            isLoading = false
        }

        override fun getItemViewType(position: Int): Int {
            return when {
                books[position].isLast -> {
                    LOADING_ITEM
                }
                else -> {
                    LIST_ITEM
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return when (viewType) {
                LIST_ITEM -> {
                    ViewHolder(
                        LayoutInflater.from(parent.context)
                            .inflate(R.layout.books_rv_layout, parent, false)
                    )
                }
                else -> {
                    LoadMorePostsViewHolder(
                        LayoutInflater.from(parent.context)
                            .inflate(R.layout.footer_recyclerview_layout, parent, false)
                    )
                }
            }
        }


    }
