package com.example.pepperinlibrary.ui.borrow_book

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.Say
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.data_loader.CustomCallback
import com.example.pepperinlibrary.network.data_loader.HttpRequest
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.Tools
import com.example.pepperinlibrary.tools.extensions.hideKeyboard
import com.example.pepperinlibrary.tools.extensions.setColor
import com.example.pepperinlibrary.tools.extensions.setGlideImage
import com.example.pepperinlibrary.ui.books.BooksModel
import com.example.pepperinlibrary.ui.splash_screen.SplashScreenActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_borrow_book.*
import kotlinx.android.synthetic.main.base_toolbar_layout.*
import kotlinx.android.synthetic.main.bottom_sheet_borrow_book_layout.*


class BorrowBookActivity : RobotActivity(), RobotLifecycleCallbacks {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>
    private lateinit var database: DatabaseReference
    private var bookId = SharedPreference.instance().getString(SharedPreference.BORROW_BOOK_ID)!!
    private var sayFuture: Future<Void>? = null
    private var say2: Say? = null
    private var say3: Say? = null
    private var say4: Say? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_borrow_book)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)
        database = FirebaseDatabase.getInstance().reference
        borrowBookDetails()
        initBottomSheet()
        initSpinners()
        clickListeners()

    }

    private fun clickListeners() {
        backButtonToolbar.setOnClickListener {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
        borrowButton.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }

        floatingActionButton.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        doneButton.setOnClickListener {
            saveUserToFirebaseDatabase()
        }


    }

    private fun initSpinners() {
        ArrayAdapter.createFromResource(
            this,
            R.array.numbers_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            numberSpinner.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.dates_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            dateSpinner.adapter = adapter
        }


    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(layoutBottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        floatingActionButton.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        floatingActionButton.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(view: View, p1: Float) {
            }
        })
    }

    private fun borrowBookDetails() {
        HttpRequest.getRequest(bookId, "1",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    d("borrowResponse", response)
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    borrowBookImageView.setGlideImage(booksModel.items[0].volumeInfo.imageLinks.thumbnail)
                    borrowTitleTV.setColor(
                        booksModel.items[0].volumeInfo.title,
                        ContextCompat.getColor(this@BorrowBookActivity, R.color.colorAccent)
                    )
                    booksModel.items[0].volumeInfo.authors.forEach {
                        borrowAuthorTV.setColor(
                            it,
                            ContextCompat.getColor(this@BorrowBookActivity, R.color.colorAccent)
                        )
                    }
                    booksModel.items.forEach {
                        borrowCountryTV.setColor(
                            it.saleInfo.country,
                            ContextCompat.getColor(this@BorrowBookActivity, R.color.colorAccent)
                        )

                    }
                }

                override fun onFailure(fail: String) {

                }
            })
    }


    private fun saveUserToFirebaseDatabase() {
        if (emailEditText.text.toString().isEmpty()) {
            emailEditText.error = "Enter Your Email"
            emailEditText.requestFocus()
            if (say3 != null)
                sayFuture = say3!!.async().run()
            return
        }
        if (PINEditText.text.toString().isEmpty()) {
            PINEditText.error = "Enter Your ID number"
            PINEditText.requestFocus()
            if (say3 != null)
                sayFuture = say3!!.async().run()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches()) {
            emailEditText.error = "Invalid Email"
            emailEditText.requestFocus()
            if (say3 != null)
                sayFuture = say3!!.async().run()
            return
        }
        if (PINEditText.length() < 6) {
            PINEditText.error = "Invalid PIN (min 6 number)"
            PINEditText.requestFocus()
            if (say3 != null)
                sayFuture = say3!!.async().run()
            return
        }
        if (numberSpinner.selectedItemPosition == 0) {
            numberSpinner.requestFocus()
            if (say4 != null)
                sayFuture = say4!!.async().run()
            return
        }
        if (dateSpinner.selectedItemPosition == 0) {
            numberSpinner.requestFocus()
            if (say4 != null)
                sayFuture = say4!!.async().run()
            return
        }
        val uid = PINEditText.text.toString()
        val ref = FirebaseDatabase.getInstance().getReference("users/$uid/$bookId")

        val user = UserModel()
        user.pin = PINEditText.text.toString()
        user.email = emailEditText.text.toString()
        user.number = numberSpinner.selectedItem.toString()
        user.date = dateSpinner.selectedItem.toString()
        user.bookId = bookId

        ref.setValue(user)
            .addOnSuccessListener {
                PINEditText.text.clear()
                emailEditText.text.clear()
                numberSpinner.setSelection(0)
                dateSpinner.setSelection(0)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                emailEditText.hideKeyboard()
                PINEditText.hideKeyboard()
                if (say2 != null)
                    say2!!.async().run()
                Handler().postDelayed({
                    Tools.startNewActivity(this, SplashScreenActivity::class.java)
                },7000)
            }

    }

    override fun onRobotFocusGained(qiContext: QiContext?) {
        val say1: Say = SayBuilder.with(qiContext)
            .withText("here we go, on the last step, click on the borrow button, and finish filling the form !")
            .build()
        say1.run()
        say2 = SayBuilder.with(qiContext)
            .withText("That's  all , now, you can take your book at home, and don't forget to bring it back on time! Have a nice book days !")
            .build()

        say3 = SayBuilder.with(qiContext)
            .withText("It's necessary to fill al the fields correctly !")
            .build()
        say4 = SayBuilder.with(qiContext)
            .withText("please, check the date, as long as you want to borrow that book !")
            .build()
    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {
    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }

}