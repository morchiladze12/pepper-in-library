package com.example.pepperinlibrary.ui.books

import android.os.Bundle
import android.os.Handler
import android.util.Log.d
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.data_loader.CustomCallback
import com.example.pepperinlibrary.network.data_loader.HttpRequest
import com.example.pepperinlibrary.network.data_loader.HttpRequest.CATEGORY
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.Tools.booksDetailsDialog
import com.example.pepperinlibrary.tools.Tools.chooseBookDialog
import com.example.pepperinlibrary.tools.Tools.startNewActivity
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import com.example.pepperinlibrary.ui.books.books_pagination.OnLoadMoreListener
import com.example.pepperinlibrary.ui.books.books_pagination.PaginationBooksAdapter
import com.example.pepperinlibrary.ui.borrow_book.BorrowBookActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_books.*
import kotlinx.android.synthetic.main.base_toolbar_layout.*


class BooksActivity : RobotActivity(), RobotLifecycleCallbacks, Runnable {

    private lateinit var adapter: PaginationBooksAdapter
    private var books = mutableListOf<BooksModel.Items>()
    private var qiChatBot: QiChatbot? = null
    private val phrases = mutableListOf<Phrase>()
    private var editablePhraseSet: EditablePhraseSet? = null
    private var moreBooks = mutableListOf<BooksModel.Items>()

    companion object {
        var newBooks = mutableListOf<BooksModel.Items>()
        var currentBook: BooksModel.Items? = null
        var currentBooksDescription = ""
    }

    var loadMoreBooks = 9

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)
        onClickListeners()
        init()
    }

    private fun init() {
        getBooks()
        val glm = GridLayoutManager(this, 3)
        glm.spanSizeLookup = object : SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (adapter.getItemViewType(position)) {
                    PaginationBooksAdapter.LOADING_ITEM -> 3
                    PaginationBooksAdapter.LIST_ITEM -> 1
                    else -> -1
                }
            }
        }

        booksRV.layoutManager = glm
        adapter = PaginationBooksAdapter(booksRV, moreBooks, loadMoreListener, customClick)
        booksRV.adapter = adapter

    }

    private val customClick = object : CustomOnClick {
        override fun onClick(position: String) {
            super.onClick(position)
            getBookDetails(position, qiChatBot)
        }
    }

    private fun onClickListeners() {
        backButtonToolbar.setOnClickListener {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
    }

    private val loadMoreListener = object :
        OnLoadMoreListener {
        override fun onLoadMore() {
            if (moreBooks.size != 0) {
                if (!moreBooks[moreBooks.size - 1].isLast) {
                    booksRV.post {
                        val booksModel = BooksModel.Items()
                        booksModel.isLast = true
                        moreBooks.add(booksModel)
                        adapter.notifyItemInserted(moreBooks.size - 1)
                        Handler().postDelayed({ getMoreBooks() }, 1000)
                    }
                }
            }
        }
    }

    private fun getBooks() {
        val category = SharedPreference.instance().getString(SharedPreference.CATEGORY)!!
        HttpRequest.getRequest(CATEGORY + category, "40",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    d("sadsadjkrenfv", response)
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    booksModel.items.forEach {
                        books.add(it)
                    }
                    for (i in 0 until loadMoreBooks) {
                        if (books.size - 1 == i)
                            break
                        moreBooks.add(books[i])
                        d("loadedBooks", books[i].volumeInfo.title)
                    }
                    books.forEach {
                        newBooks.add(it)
                    }
                    adapter.notifyDataSetChanged()
                }

                override fun onFailure(fail: String) {

                }
            })
    }


    override fun onRobotFocusGained(qiContext: QiContext) {
        val say1 = SayBuilder.with(qiContext)
            .withText("There are some books that i can suggest you now, Just tell me the favourite book's name, and I will tell you the details")
            .build()
        say1.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.books)
            .build()

        qiChatBot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic)
            .build()

        val booksExecutor = hashMapOf<String, QiChatExecutor>()
        booksExecutor["books_executor"] = BooksExecutor(qiContext)
        qiChatBot!!.executors = booksExecutor

        val chatBot = mutableListOf<Chatbot>()
        chatBot.add(qiChatBot!!)

        val chat: Chat = ChatBuilder.with(qiContext).withChatbot(qiChatBot).build()

        editablePhraseSet = qiChatBot!!.dynamicConcept("books")

        moreBooks.forEach {
            phrases.add(Phrase(it.volumeInfo.title))
        }
        editablePhraseSet!!.addPhrases(phrases)

        qiChatBot?.addOnBookmarkReachedListener { bookmark: Bookmark ->
            when (bookmark.name) {
                "BOOK" -> {
                    if (currentBook!!.id.isNotEmpty()) {
                        this.runOnUiThread {
                            chooseBookDialog(
                                this,
                                currentBook,
                                qiChatBot,
                                this
                            )
                        }
                    }
                }
                "YES" -> {
                    getBookDetails(currentBook!!.id, qiChatBot)
                    val booksDetailsVariable: QiChatVariable = qiChatBot!!.variable("books_details")
                    currentBooksDescription = currentBook!!.volumeInfo.description
                    booksDetailsVariable.value = currentBooksDescription
                    booksDetailsVariable.addOnValueChangedListener {
                        this.runOnUiThread {
                            d("sddsdsdsds", currentBooksDescription)
                        }
                    }
                }
                "SCROLL_DOWN" -> {
                    runOnUiThread {
                        run {
                            booksRV.smoothScrollToPosition(moreBooks.size - 1)
                        }
                    }
                    editablePhraseSet = qiChatBot!!.dynamicConcept("books")
                    phrases.clear()
                    moreBooks.forEach {
                        phrases.add(Phrase(it.volumeInfo.title))
                    }
                    editablePhraseSet!!.addPhrases(phrases)
                }
                "SCROLL_UP" -> {
                    runOnUiThread {
                        run {
                            booksRV.smoothScrollToPosition(0)
                        }
                    }
                }
                "NO" -> {
                    currentBook = null
                }
                "EXIT" -> {
                    currentBook = null
                }
                "BACK" -> {
                    books.clear()
                    this.finish()
                }
                "BORROW_BOOK" ->{
                    SharedPreference.instance().saveString(SharedPreference.BORROW_BOOK_ID, currentBook!!.id)
                    startNewActivity(this,BorrowBookActivity::class.java)                }

            }
        }

        chat.async().run()
    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {

    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }


    private fun getBookDetails(bookId: String, qiChatbot: QiChatbot?) {
        HttpRequest.getRequest(bookId, "1",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    d("bookssResponse", response)
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    booksDetailsDialog(
                        this@BooksActivity,
                        booksModel.items[0],
                        qiChatbot,
                        this@BooksActivity
                    )

                }

                override fun onFailure(fail: String) {

                }
            })
    }

    override fun run() {
    }

    private fun getMoreBooks() {
        if (loadMoreBooks + 6 < books.size - 1) {
            val lastPosition = moreBooks.size
            if (lastPosition > 0) {
                moreBooks.removeAt(moreBooks.size - 1)
                adapter.notifyItemRemoved(moreBooks.size - 1)
            }
            adapter.setLoaded()
            for (i in loadMoreBooks until loadMoreBooks + 6) {
                moreBooks.add(books[i])
                d("loadedBooks111", books[i].volumeInfo.title)
                if (i == books.size - 1)
                    break
            }
            if (moreBooks.size > 1) {
                adapter.notifyItemMoved(
                    lastPosition,
                    moreBooks.size - 1
                )
            }
            adapter.notifyDataSetChanged()
            loadMoreBooks += 6
        }
        if (loadMoreBooks > books.size - 1) {
            loadMoreBooks = books.size - 1
        }
    }

}

