package com.example.pepperinlibrary.ui.scan_book

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Log.d
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.data_loader.CustomCallback
import com.example.pepperinlibrary.network.data_loader.HttpRequest
import com.example.pepperinlibrary.tools.Tools
import com.example.pepperinlibrary.ui.books.BooksModel
import com.example.pepperinlibrary.ui.splash_screen.SplashScreenActivity
import com.google.android.gms.vision.barcode.Barcode
import com.google.gson.Gson
import com.softbankrobotics.dx.peppercodescanner.BarcodeReaderActivity
import com.softbankrobotics.dx.peppercodescanner.BarcodeReaderActivity.KEY_SCAN_OVERLAY_VISIBILITY
import kotlinx.android.synthetic.main.activity_scan_book.*

class ScanBookActivity : RobotActivity(), RobotLifecycleCallbacks {


    companion object {
        private var qiChatBot: QiChatbot? = null
        private var sayFuture: Future<Void>? = null
        private var say2: Say? = null
        var say3: Say? = null
        const val BARCODE_READER_ACTIVITY_REQUEST = 10
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_book)
        QiSDK.register(this, this)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)

        scanQRCodeButton.setOnClickListener {
            val launchIntent = Intent(this, BarcodeReaderActivity::class.java)
            launchIntent.putExtra(KEY_SCAN_OVERLAY_VISIBILITY, false)
            startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            d("barcodeSuccessNo", "Scan error")
            return
        }

        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
            val barcode: Barcode? =
                data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE)
            getBookDetails(barcode?.rawValue ?: "")
            d("barcodeSuccess", barcode!!.rawValue)
        }
    }


    override fun onBackPressed() {
        this.finish()
    }

    private fun getBookDetails(bookId: String) {
        HttpRequest.getRequest(bookId, "1",
            object : CustomCallback {
                override fun onResponse(response: String) {
                    d("responseDet", response)
                    val booksModel = Gson().fromJson(
                        response,
                        BooksModel::class.java
                    )
                    if (booksModel.items.isNotEmpty()) {
                        d("sadsda", sayFuture.toString())
                        if (say2 != null) {
                            sayFuture = say2!!.async().run()
                        }

                        Tools.booksDetailsDialog(
                            this@ScanBookActivity,
                            booksModel.items[0],
                            qiChatBot,
                            this@ScanBookActivity
                        )
                    }

                }

                override fun onFailure(fail: String) {

                }
            })
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {
        val say1 = SayBuilder.with(qiContext)
            .withText("just click on the button, and scan your book barcode !")
            .build()
        say1.async().run()

        say2 = SayBuilder.with(qiContext)
            .withText("Here is your book, Now you can Borrow it !")
            .build()

        say3 = SayBuilder.with(qiContext)
            .withText("unfortunately, something went wrong, you can try again !")
            .build()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.books)
            .build()

        qiChatBot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic)
            .build()

        val chat: Chat = ChatBuilder
            .with(qiContext)
            .withChatbot(qiChatBot)
            .build()

        qiChatBot!!.addOnBookmarkReachedListener { bookmark: Bookmark ->
            when (bookmark.name) {
                "BACK" -> {
                    this.finish()
                }
                "SCAN_BOOK" -> {
                    val launchIntent = Intent(this, BarcodeReaderActivity::class.java)
                    launchIntent.putExtra(KEY_SCAN_OVERLAY_VISIBILITY, false)
                    startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST)
                }
            }
        }

        chat.async().run()
    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {

    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }


}