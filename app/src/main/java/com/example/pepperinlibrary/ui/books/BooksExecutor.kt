package com.example.pepperinlibrary.ui.books

import android.util.Log
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor

class BooksExecutor(context: QiContext) : BaseQiChatExecutor(context) {
    override fun runWith(params: List<String>) {
        if (params.isNotEmpty()) {
            BooksActivity.currentBook = null
            BooksActivity.newBooks.forEach {
                if (it.volumeInfo.title == params[0]) {
                    BooksActivity.currentBook = it
                }
            }
        }
    }

    override fun stop() {
        Log.d("BooksExecutor", "BooksExecutor stopped")
    }
}