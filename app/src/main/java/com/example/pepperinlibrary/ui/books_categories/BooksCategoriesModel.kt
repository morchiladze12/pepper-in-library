package com.example.pepperinlibrary.ui.books_categories

import com.google.gson.annotations.SerializedName

class BooksCategoriesModel(var category: String, var imgUrl: String)