package com.example.pepperinlibrary.ui.books


class BooksModel {
    var items = mutableListOf<Items>()

    class Items {
        var id = ""
        var volumeInfo = VolumeInfo()

        class VolumeInfo {
            var title = ""
            var subtitle = ""
            var authors = mutableListOf<String>()
            var publisher = ""
            var publishedDate = ""
            var description = ""
            var pageCount = 0
            var categories = mutableListOf<String>()
            var averageRating = 0.0
            var ratingsCount = 0
            var imageLinks = ImageLinks()

            class ImageLinks {
                var thumbnail = ""
            }

            var language = ""
        }

        var saleInfo = SaleInfo()

        class SaleInfo {
            var country = ""
            var saleability = ""
            var isEbook = true
        }

        var isLast = false

    }


}
