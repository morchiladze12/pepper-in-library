package com.example.pepperinlibrary.ui.books_categories

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.SayBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.Tools.startNewActivity
import com.example.pepperinlibrary.ui.callbacks.CustomOnClick
import com.example.pepperinlibrary.ui.books.BooksActivity
import kotlinx.android.synthetic.main.activity_categories_books.*
import kotlinx.android.synthetic.main.base_toolbar_layout.*


class BooksCategoriesActivity : RobotActivity(), RobotLifecycleCallbacks {

    private lateinit var adapterBooks: BooksCategoriesRecyclerViewAdapter
    var categories = mutableListOf<BooksCategoriesModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.IMMERSIVE)
        setContentView(R.layout.activity_categories_books)
        init()
        QiSDK.register(this, this)
    }

    private fun init() {
        backButtonToolbar.setOnClickListener {
            super.onBackPressed()
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
        }
        categories = mutableListOf(
            BooksCategoriesModel(
                "Drama",
                "https://wrcitytimes.com/wp-content/uploads/2020/01/drama-image.jpg"
            ),
            BooksCategoriesModel(
                "Romance",
                "https://i.pinimg.com/564x/dd/a5/2b/dda52b332dbb77005c2779efa62b8cd9.jpg"
            ),
            BooksCategoriesModel(
                "Mystery",
                "https://media.npr.org/assets/img/2012/12/17/mystery-list_custom-9c58a855ae7747d9f76edcebbf2c6e50d482a41d-s800-c85.jpg"
            ),
            BooksCategoriesModel(
                "Horror",
                "https://cdn2.iconfinder.com/data/icons/halloween-symbols/64/holidays_pumpkin-512.png"
            ),
            BooksCategoriesModel(
                "Thriller",
                "https://art-sheep.com/wp-content/uploads/2015/03/jamesbond99.jpg"
            ),
            BooksCategoriesModel(
                "Science",
                "https://thumbs-prod.si-cdn.com/s-jZTk0XtVmp-89MlOgFXqaAVe4=/fit-in/1600x0/https://public-media.si-cdn.com/filer/29/0f/290fb8c0-1872-46e5-8c12-235742905def/science_smithsonian_magazine_booklist_2019.png"
            ),
            BooksCategoriesModel(
                "Fiction",
                "https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/WL_FlyingH.jpg?itok=uJ3BKhho"
            ),
            BooksCategoriesModel(
                "Art",
                "https://i.etsystatic.com/13726973/r/il/999bdd/1424060518/il_570xN.1424060518_k0dd.jpg"
            ),
            BooksCategoriesModel(
                "Action",
                "https://i.ytimg.com/vi/krhmi78UYjw/maxresdefault.jpg"
            ),
            BooksCategoriesModel(
                "Adventure",
                "https://image.freepik.com/free-vector/vector-illustration-flat-landscape-mountain-hiking-background_68155-10.jpg"
            ),
            BooksCategoriesModel(
                "Classics",
                "https://www.learningliftoff.com/wp-content/uploads/2014/04/ClassicBooks-730x390.jpg"
            ),
            BooksCategoriesModel(
                "Comic",
                "https://www.verdict.co.uk/wp-content/uploads/2017/09/get-into-comic-books.jpg"
            ),
            BooksCategoriesModel(
                "Novel",
                "https://image.freepik.com/free-photo/vintage-novel-books-with-bouquet-flowers-old-wood-background-concept-nostalgic-remembrance-spring-vintage-background_1484-646.jpg"
            ),
            BooksCategoriesModel(
                "Crime",
                "https://miro.medium.com/max/1200/1*SVUOSDvBriNryyrOXpHttA.png"
            ),
            BooksCategoriesModel(
                "Detective",
                "https://blog.samuel-windsor.co.uk/wp-content/uploads/2017/01/SW-Detective-quiz-background-image-e1484646324989.jpg"
            ),
            BooksCategoriesModel(
                "Fantasy",
                "https://toppng.com/uploads/preview/magician-dragon-art-magic-fantasy-115703491022qmewzqnh6.jpg"
            ),
            BooksCategoriesModel(
                "Historical Fiction",
                "https://rosieamber.files.wordpress.com/2015/11/92521-history2b2.jpg"
            ),
            BooksCategoriesModel(
                "Literary Fiction",
                "https://wallpaperaccess.com/full/127346.jpg"
            ),
            BooksCategoriesModel(
                "Children",
                "https://i.pinimg.com/originals/23/66/67/2366671c14b06f515cdaf15bce7e0617.jpg"
            ),
            BooksCategoriesModel(
                "Biographies",
                "https://d.newsweek.com/en/full/847580/00.jpg?w=737&f=542fd641dccd56211f2a506fbff5aa24"
            ),
            BooksCategoriesModel(
                "Kitchen",
                "https://i.pinimg.com/originals/38/62/4c/38624cb365819fb5e8aeb1b57e7fb3af.jpg"
            ),
            BooksCategoriesModel(
                "History",
                "https://ahabiblemoments.files.wordpress.com/2020/03/old-history-researfch.jpg?w=584"
            ),
            BooksCategoriesModel(
                "Poetry",
                "https://s3.amazonaws.com/newsimg.furman.edu/wp-content/uploads/2016/05/27160830/poetry-pen-and-quill-funews-big.jpg"
            ),
            BooksCategoriesModel(
                "Essays",
                "https://springsadvertiser.co.za/wp-content/uploads/sites/28/2018/08/letter-writing-pen.jpg"
            )
        )
        adapterBooks = BooksCategoriesRecyclerViewAdapter(categories, object :
            CustomOnClick {
            override fun onClick(position: String) {
                super.onClick(position)
                SharedPreference.instance().saveString(SharedPreference.CATEGORY, position)
                startNewActivity(this@BooksCategoriesActivity, BooksActivity::class.java)
            }
        })
        booksCategoriesRV.layoutManager = GridLayoutManager(this, 4)
        booksCategoriesRV.adapter = adapterBooks

    }

    override fun onRobotFocusGained(qiContext: QiContext) {
        val say: Say = SayBuilder.with(qiContext)
            .withText("What  is  your  favourite  book's  Category ?")
            .build()
        say.run()

        val topic: Topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.choose_categories)
            .build()

        val qiChatBot: QiChatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic)
            .build()

        val executors = hashMapOf<String, QiChatExecutor>()
        executors["categories"] = BooksCategoriesExecutor(qiContext)
        qiChatBot.executors = executors
        val chatBot = mutableListOf<Chatbot>()
        chatBot.add(qiChatBot)

        val chat: Chat = ChatBuilder
            .with(qiContext)
            .withChatbot(qiChatBot)
            .build()

        qiChatBot.addOnBookmarkReachedListener { bookmark: Bookmark ->
            when (bookmark.name) {
                "BACK" -> {
                    super.finish()
                }
                "SCROLL_UP" -> {
                    runOnUiThread {
                        run {
                            booksCategoriesRV.smoothScrollToPosition(0)
                        }
                    }
                }
                "SCROLL_DOWN" -> {
                    runOnUiThread {
                        run {
                            booksCategoriesRV.smoothScrollToPosition(categories.size-1)
                        }
                    }
                }
            }
        }
        chat.async().run()

    }

    override fun onRobotFocusLost() {
    }

    override fun onRobotFocusRefused(reason: String?) {
    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }
}