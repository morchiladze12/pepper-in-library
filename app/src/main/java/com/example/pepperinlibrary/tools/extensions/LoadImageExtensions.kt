package com.example.pepperinlibrary.tools.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.pepperinlibrary.App
import com.example.pepperinlibrary.R

fun ImageView.setGlideImage(img: String) {
    Glide.with(App.context!!).load(img).placeholder(R.mipmap.ic_no_book_image).into(this)
}