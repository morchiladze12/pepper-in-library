package com.example.pepperinlibrary.tools

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.example.pepperinlibrary.R
import com.example.pepperinlibrary.network.shared_preference.SharedPreference
import com.example.pepperinlibrary.tools.extensions.setColor
import com.example.pepperinlibrary.tools.extensions.setGlideImage
import com.example.pepperinlibrary.tools.extensions.visible
import com.example.pepperinlibrary.ui.books.BooksModel
import com.example.pepperinlibrary.ui.borrow_book.BorrowBookActivity
import kotlinx.android.synthetic.main.books_details_dialog_layout.*
import kotlinx.android.synthetic.main.choose_book_dialog_layout.*


object Tools {
    fun <T> startNewActivity(activity: Activity, class_: Class<T>) {
        val intent = Intent(activity, class_)
        activity.startActivity(intent)
        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    fun chooseBookDialog(
        context: Context?,
        currentBook: BooksModel.Items?,
        chatBot: QiChatbot?,
        activity: Activity
    ) {
        val dialog = Dialog(context!!)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.choose_book_dialog_layout)

        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams
        dialog.dialogBookImageView.setGlideImage(currentBook!!.volumeInfo.imageLinks.thumbnail)
        dialog.noDialogButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.yesDialogButton.setOnClickListener {
            booksDetailsDialog(context, currentBook, chatBot, activity)
            dialog.dismiss()
        }
        val thread = Thread(Runnable {
            chatBot?.addOnBookmarkReachedListener { bookmark ->
                when (bookmark.name) {
                    "NO" -> {
                        dialog.dismiss()
                    }
                    "YES" -> {
                        dialog.dismiss()

                    }
                    "EXIT" -> {
                        if (dialog.isShowing)
                            dialog.dismiss()
                    }

                }
            }
        })
        thread.start()

        if (!(context as Activity).isFinishing) {
            dialog.show()
        }
    }


    fun booksDetailsDialog(
        context: Context?,
        currentBook: BooksModel.Items?,
        chatBot: QiChatbot?,
        activity: Activity
    ) {
        val dialog = Dialog(context!!)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.books_details_dialog_layout)
        val params: ViewGroup.LayoutParams = dialog.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = params as WindowManager.LayoutParams

        dialog.titleDescriptionTVDialog.text = currentBook!!.volumeInfo.title
        var authors = ""
        if (currentBook.volumeInfo.authors.isNotEmpty()){
            currentBook.volumeInfo.authors.forEach {
                authors += " $it "
            }
            dialog.authorDescriptionTVDialog.setColor(authors,
                ContextCompat.getColor(context, R.color.colorAccent)
            )
        }

        dialog.publisherDescriptionTVDialog.setColor(currentBook.volumeInfo.publisher,
            ContextCompat.getColor(context, R.color.colorAccent)
        )

        dialog.publishDateDescriptionTVDialog.setColor(currentBook.volumeInfo.publishedDate,
            ContextCompat.getColor(context, R.color.colorAccent)
        )
        dialog.pagesDescriptionTVDialog.setColor(currentBook.volumeInfo.pageCount.toString(),
            ContextCompat.getColor(context, R.color.colorAccent)
        )

        dialog.languageDescriptionTVDialog.setColor(currentBook.volumeInfo.language,
            ContextCompat.getColor(context, R.color.colorAccent)
        )

        val category = SharedPreference.instance().getString(SharedPreference.CATEGORY)!!

        if (currentBook.volumeInfo.categories.size == 1) {
            if (currentBook.volumeInfo.categories[0] != category) {
                dialog.bookGenres1.text = currentBook.volumeInfo.categories[0]
                dialog.bookGenres2.text = category
                dialog.bookGenres1.visible(true)
                dialog.bookGenres2.visible(true)
            } else {
                dialog.bookGenres1.text = currentBook.volumeInfo.categories[0]
                dialog.bookGenres1.visible(true)
            }
        } else if (currentBook.volumeInfo.categories.size > 1) {
            dialog.bookGenres1.text = currentBook.volumeInfo.categories[0]
            dialog.bookGenres2.text = currentBook.volumeInfo.categories[1]
            dialog.bookGenres1.visible(true)
            dialog.bookGenres2.visible(true)

        }
        dialog.borrowButton.setOnClickListener {
            SharedPreference.instance().saveString(SharedPreference.BORROW_BOOK_ID, currentBook.id)
            startNewActivity(activity,BorrowBookActivity::class.java)
        }

        dialog.ratingBarDialog.rating = currentBook.volumeInfo.averageRating.toFloat()
        dialog.descriptionTVDialog.text = currentBook.volumeInfo.description
        dialog.booksDetailImageViewDialog.setGlideImage(currentBook.volumeInfo.imageLinks.thumbnail)
        dialog.dialogExitButton.setOnClickListener {
            dialog.dismiss()
        }

        val thread = Thread(Runnable {

            chatBot?.addOnBookmarkReachedListener { bookmark ->
                when (bookmark.name) {
                    "EXIT" -> {
                        if (dialog.isShowing)
                            dialog.dismiss()
                    }
                }
            }
        })

        thread.start()

        if (!(context as Activity).isFinishing) {
            dialog.show()
        }

    }

}