package com.example.pepperinlibrary.tools.extensions

import android.view.View

fun View.visible(boolean: Boolean){
    visibility = if (boolean) View.VISIBLE
    else View.GONE
}