package com.example.pepperinlibrary.network.shared_preference

import android.content.Context
import android.content.SharedPreferences
import com.example.pepperinlibrary.App

class SharedPreference {
    companion object {
        const val CATEGORY = "CATEGORY"
        const val BORROW_BOOK_ID = "BOOK_ID"

        private var sharedPreference: SharedPreference? = null
        fun instance(): SharedPreference {
            if (sharedPreference == null) {
                sharedPreference =
                    SharedPreference()
            }
            return sharedPreference!!
        }
    }


    private val sharedPreferences: SharedPreferences by lazy {
        App.context!!.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    fun saveString(key:String,value:String){
        editor.putString(key,value)
        editor.apply()
    }

    fun getString(key: String) = sharedPreferences.getString(key,"")

    fun clear(): SharedPreferences.Editor = editor.clear()

    fun delete(key: String){
        if (sharedPreferences.contains(key)){
            editor.remove(key)
        }
        editor.apply()
    }
}