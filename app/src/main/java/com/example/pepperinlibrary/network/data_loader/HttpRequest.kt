package com.example.pepperinlibrary.network.data_loader

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

object HttpRequest {
    private const val URL = "https://www.googleapis.com/"
    const val QUERY_PARAM = "q"
    const val MAX_RESULTS = "maxResults"
    const val PRINT_TYPE = "printType"
    const val CATEGORY = "subject:"

    private var retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiServices::class.java
    )

    fun getRequest(queryString: String, maxResults: String, customCallback: CustomCallback) {
        val call = service.getBooksRequest(queryString, maxResults)

        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) customCallback.onResponse(response.body().toString())
                else if (response.code() == 404)
                    customCallback.onResponse("No books found")
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                customCallback.onFailure(t.toString())
            }

        })
    }
}

