package com.example.pepperinlibrary.network.data_loader

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {
    @GET("books/v1/volumes")
    fun getBooksRequest(
        @Query(HttpRequest.QUERY_PARAM) queryParam: String,
        @Query(HttpRequest.MAX_RESULTS) maxResults: String,
        @Query(HttpRequest.PRINT_TYPE) printType: String = "books"
    ): Call<String>

}