package com.example.pepperinlibrary.network.data_loader

interface CustomCallback {
    fun onResponse(response: String)
    fun onFailure(fail: String)

}